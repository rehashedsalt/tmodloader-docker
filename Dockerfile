# Args
ARG TMOD_VERSION=2023.11.3.3

ARG UID=1521
ARG USER=terraria
ARG GID=1521
ARG GROUP=terraria

# The first stage unpacks and prepares a tModLoader server
FROM ubuntu:focal as build
ARG TMOD_VERSION
WORKDIR /terraria-server

# Install some os dependencies
RUN apt-get update && \
	apt-get install -y build-essential curl unzip
COPY config.txt start-server.sh ./
# Obtain and decompress the release tarball to pwd
# We also remove some extraneous x86 binaries and modify perms
RUN curl -LO "https://github.com/tModLoader/tModLoader/releases/download/v${TMOD_VERSION}/tModLoader.zip" && \
	unzip tModLoader.zip && \
	rm -r tModLoader.zip && \
	ls -alh && \
	chmod u+x *tModLoader*

# The second stage packs the whole thing together
FROM ubuntu:focal AS final
ARG UID
ARG USER
ARG GID
ARG GROUP
WORKDIR /terraria-server
VOLUME /terraria
VOLUME /terraria-server/tModLoader-Logs

# OS stuff
RUN apt-get update && \
	apt-get install -y curl mono-complete screen
RUN addgroup --gid "${GID}" "${GROUP}" && \
	adduser --home /home/terraria --shell /bin/bash --disabled-password --uid "${UID}" --gid "${GID}" "${USER}" && \
	ln -s /home/terraria/.local/share/Terraria /terraria
# The server binary resides in /terraria-server
# Data will reside in /terraria, which should be mounted
COPY --from=build /terraria-server ./
RUN chown -R "${USER}:${GROUP}" ./
USER $USER
CMD [ "bash", "start-server.sh" ]
EXPOSE 7777
