#!/bin/bash
#
# start-server.sh
# Copyright (C) 2022 Jacob Babor <jacob@babor.tech>
#
# Distributed under terms of the MIT license
#
set -e
server="/terraria-server/start-tModLoaderServer.sh"

# Short and sweet; let's start a server up and add some cleanup if we need it
cleanup() {
	screen -p 0 -S terraria -X stuff "say Shutting down...^M"
	screen -p 0 -S terraria -X stuff "exit^M"
}
trap cleanup EXIT

# But first, a banner
echo "To see the server console, execute this command in the container:"
echo "  screen -r terraria"
# And now we run the server in a blocking fashion
screen -DmS terraria "$server" -nosteam -config config.txt
